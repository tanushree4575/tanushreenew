// back to cart function


function goBackToCart() {
    // Navigate back to "shop.html" without page refresh
    window.location.href = "shop.html";
}



// To get Total from the Cart to the Payments page

document.addEventListener("DOMContentLoaded", function() {
    // Retrieve the total from localStorage using the same key
    var total = parseFloat(localStorage.getItem("cartTotal")) || 0;

    // Update the displayed total
    var totalElement = document.getElementById("total-price");
    if (totalElement) {
        totalElement.textContent = "$" + total.toFixed(2);
    }
    // console.log (totalElement )
});
// -----------------------------------------------------------------------


// Initialize the coupon code input and apply button

const couponCodeInput = document.getElementById("coupon-code");
const applyCouponButton = document.getElementById("apply-coupon");
const totalElement = document.getElementById("total-price");


applyCouponButton.addEventListener("click", function() {
    // Get the entered coupon code from the input field
    const enteredCouponCode = couponCodeInput.value;

    // Here, you can add logic to validate and apply the coupon
    if (enteredCouponCode == "TANU") {
        // Apply the coupon logic 
        // alert("Coupon applied successfully! You got a discount.");
        var total = parseFloat(totalElement.textContent.replace("$", ""));
        var newTotal = total - 100;

        // Update the total price with the new total
        totalElement.textContent = "$" + newTotal.toFixed(2);
      
    } else {
        alert("Invalid coupon code. Please try again.");
    }

    // Clear the input field
    couponCodeInput.value = "";
    
});
// ----------------------------------------------------------------------------



// JS for Popup Box

let popup = document.getElementById("popup");
// const popupCheck = document.querySelector("pop-check");


function openPopup(title, message) {
    console.log("Popup opened");
    popupTitle.textContent = title;
    popupMessage.textContent = message;
    popup.classList.add("open-popup");

}


function closePopup() {
    console.log("Popup closed");
    popup.classList.remove("open-popup")

}


// for verifying the card details

const paynowbutton = document.getElementById("pay-button")
const cardNumberInput = document.getElementById("cardnumber");
const cardCvvInput = document.getElementById("cvv");

paynowbutton.addEventListener("click", function() {
console.log("Pay Now button clicked");
const enterCardnumber = cardNumberInput.value;
const enterCvv = cardCvvInput.value;

if (enterCardnumber == 123456 && enterCvv == 321) {
    // alert("Payment Successful")
    openPopup("Payment Successful", "Thank you for your payment!");

}else {
    // alert("Invalid Card Number")
    openPopup("Payment Error", "Invalid Card Number. Please try again.");
}
cardNumberInput.value = " ";
cardCvvInput.value = " ";


});

document.getElementById("popup").addEventListener("click", function (event) {
    if (event.target.id === "ok-btn") {
        closePopup();
    }
});




