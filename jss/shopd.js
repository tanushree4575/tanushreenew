// cart
let cartIcon = document.querySelector("#cart-icon");
let cart = document.querySelector(".cart");
let cartClose = document.querySelector("#cart-close");
// open cart
document.addEventListener("DOMContentLoaded", function() {
  let cartIcon = document.querySelector("#cart-icon");
  if (cartIcon) {
      cartIcon.onclick = () => {
          cart.classList.add("active");
      };
  }
});

// cart close
if (cartClose) {
  cartClose.onclick = () => {
    cart.classList.remove("active");
  };
}
// cartIcon.onclick = () => {
//   cart.classList.add("active");
// };
// // close cart
// cartClose.onclick = () => {
//   cart.classList.remove("active");
// };

// Cart working Js
if (document.readyState == "loading") {
  document.addEventListener("DOMContentLoaded", ready);
} else {
  ready();
}

// Making Function
function ready() {
  // Remove items from cart
  var removeCartButtons = document.getElementsByClassName("cart-remove");
  for (var i = 0; i < removeCartButtons.length; i++) {
    var button = removeCartButtons[i];
    button.addEventListener("click", removeCartItem);
  }
  // Quanttity changes
  var quantityInputs = document.getElementsByClassName("cart-quantity");
  for (var i = 0; i < quantityInputs.length; i++) {
    var input = quantityInputs[i];
    input.addEventListener("change", quantityChanged);
  }
  // Add to Cart
  var addCart = document.getElementsByClassName("add-btn");
  for (var i = 0; i < addCart.length; i++) {
    var buttonAdd = addCart[i];
    buttonAdd.addEventListener("click", addCartClicked);
  }

}


// Remove items from cart
function removeCartItem(event) {
  var buttonClicked = event.target;
  buttonClicked.parentElement.remove();
  updateTotal();
}

// quantity changes
function quantityChanged(event) {
  var input = event.target;
  if (isNaN(input.value) || input.value <= 0) {
    input.value = 1;
  }
  updateTotal();
}

//  Add TO Cart Function
function addCartClicked(event) {
  var button = event.target;
  var shopProducts = button.parentElement;
  var title = shopProducts.getElementsByClassName("product-name")[0].innerText;
  var price = shopProducts.getElementsByClassName("price")[0].innerText;
  var productImg = shopProducts.getElementsByClassName("product-img")[0].src;
  // console.log(title, price, productImg)
  addProductToCart(title, price, productImg);
  updateTotal();
}

function addProductToCart(title, price, productImg) {
  var cartShopBox = document.createElement('div');
  cartShopBox.classList.add("cart-box");
  var cartItems = document.getElementsByClassName("cart-content")[0];
  var cartitemsNames = cartItems.getElementsByClassName("cart-product-title");
  for (var i = 0; i < cartitemsNames.length; i++) {
    // alert("You have already add this item to cart");
//    return;
  }

var cartBoxContent = ` 
                        <img src="${productImg}" alt="" class="cart-img">
                        <div class="detail-box">
                            <div class="cart-product-title">${title}</div>
                                <div class="cart-price">${price}</div>
                                    <input type="number" value = "1" class="cart-quantity">
                        </div>   
                                <!-- remove cart -->
                                <i class="fa-solid fa-trash cart-remove"></i>`;
cartShopBox.innerHTML = cartBoxContent;
cartItems.append(cartShopBox);
cartShopBox
  .getElementsByClassName("cart-remove")[0]
  .addEventListener("click", removeCartItem);
cartShopBox
  .getElementsByClassName("cart-quantity")[0]
  .addEventListener("change", quantityChanged);
  console.log(cartBoxContent)
}

// Update total
function updateTotal() {
  var cartContent = document.getElementsByClassName("cart-content")[0];
  var cartBoxes = cartContent.getElementsByClassName("cart-box");
  var total = 0;
  for (var i = 0; i < cartBoxes.length; i++) {
    var cartBox = cartBoxes[i];
    var priceElement = cartBox.getElementsByClassName("cart-price")[0];
    var quantityElement = cartBox.getElementsByClassName("cart-quantity")[0];
    var priceText = priceElement.innerText.replace("$", "").trim();
    var price = parseFloat(priceText);
    var quantity = quantityElement.value;
    total = total + price * quantity;
    console.log(total);

  // Store the total in localStorage with a specific key
    localStorage.setItem("cartTotal", total);
   

    // if price contain some Cents values
    total = Math.round(total * 100) / 100;

    document.getElementsByClassName("total-price")[0].innerText = "$" + total;
  }
}
